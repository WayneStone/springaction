package com.wayne;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.*;
import com.wayne.test.test2;
import java.text.ParseException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        Thread t1 = new Thread() {

            @Override
            public void run() {
                try {
                    DateFormatThreadUtil.parse("2017-01-01 00:00:00",DateFormatThreadUtil.yyyy_MM_dd_HH_mm_ss);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
        Thread t2 = new Thread() {

            @Override
            public void run() {
                try {
                    DateFormatThreadUtil.parse("2017-01-01 00:00:00",DateFormatThreadUtil.yyyy_MM_dd_HH_mm_ss);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
        Thread t3 = new Thread() {

            @Override
            public void run() {
                try {
                    DateFormatThreadUtil.parse("2017-01-01 00:00:00",DateFormatThreadUtil.yyyy_MM_dd_HH_mm_ss);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
        Thread t4 = new Thread() {

            @Override
            public void run() {
                try {
                    DateFormatThreadUtil.parse("2017-01-01 00:00:00",DateFormatThreadUtil.yyyy_MM_dd_HH_mm_ss);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
        Thread t5 = new Thread() {

            @Override
            public void run() {
                try {
                    DateFormatThreadUtil.parse("2017-01-01 00:00:00",DateFormatThreadUtil.yyyy_MM_dd_HH_mm_ss);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
        Thread t6 = new Thread() {

            @Override
            public void run() {
                try {
                    DateFormatThreadUtil.parse("2017-01-01 00:00:00",DateFormatThreadUtil.yyyy_MM_dd_HH_mm_ss);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
        Thread t7 = new Thread() {

            @Override
            public void run() {
                try {
                    DateFormatThreadUtil.parse("2017-01-01 00:00:00",DateFormatThreadUtil.yyyy_MM_dd_HH_mm_ss);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
        Thread t8 = new Thread() {

            @Override
            public void run() {
                try {
                    DateFormatThreadUtil.parse("2017-01-01 00:00:00",DateFormatThreadUtil.yyyy_MM_dd_HH_mm_ss);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };

//        System.out.println("单线程执行: ");
//        ExecutorService exec = Executors.newFixedThreadPool(1);
//        exec.execute(t1);
//        exec.execute(t2);
//        exec.execute(t3);
//        exec.execute(t4);
//        exec.execute(t5);
//        exec.execute(t6);
//        exec.shutdown();
//
//        sleep(1000);

        System.out.println("双线程执行: ");
        ExecutorService exec2 = Executors.newFixedThreadPool(2);
        exec2.execute(t1);
        exec2.execute(t2);
        exec2.execute(t3);
        exec2.execute(t4);
        exec2.execute(t5);
        exec2.execute(t6);
        exec2.execute(t7);
        exec2.execute(t8);
        exec2.shutdown();

      //Date testa=  DateFormatThreadUtil.parse("2017-01-01 04:05:00",DateFormatThreadUtil.yyyy_MM_dd_HH_mm_ss);
    //System.out.print(testa);
//        System.out.println( "Hello World!" );
//        String a="1234.2";
//        BigDecimal b=new BigDecimal(a);
//        //b.setScale(1);
//        //System.out.println(b.scale());
//
//        Map<String,Object> map=new TreeMap<>();
//        map.put("a","ok");
//        map.put("b","12");
//
//
//
//        List<Map.Entry<String,Object>> list=new ArrayList<>(map.entrySet());
//        Collections.sort(list, new Comparator<Map.Entry<String, Object>>() {
//            @Override
//            public int compare(Map.Entry<String, Object> o1, Map.Entry<String, Object> o2) {
//                return o1.getKey().compareTo(o2.getKey());
//            }
//        });

    }

    public static void testfan(String errormsg,boolean b,int d){
        errormsg="ok";
        b=true;
        d=10;
    }

    /**
     * 字符串转换成日期
     * @param str
     * @return date
     */
    public static Date StrToDate(String str) {

        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = null;
        try {
            date = format.parse(str);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }

    private static void sleep(long millSec) {
        try {
            TimeUnit.MILLISECONDS.sleep(millSec);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
