package com.wayne.soundsystem;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import com.wayne.test.test2;

/**
 * Created by shitou on 17/4/4.
 */

/**
 * @ComponentScan注解能够在Spring中启用组件扫描（如果没有其他配置，默认会扫描与配置类相同的包）
 * @Configuration注解表明这个类是一个配置类，该类应该包含在Spring应用上下文中如何创建bean的细节
 */
@Configuration
@ComponentScan(basePackageClasses = {SgtPeppers.class})
public class CDPlayerConfig {
}
