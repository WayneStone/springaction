package com.wayne.soundsystem;
import org.springframework.stereotype.Component;

import javax.inject.Named;
/**
 * Created by shitou on 17/4/4.
 */

/**
 * @Component注解表明该类会作为组件类，并告知Spring要为这个类创建bean
 * 大部分场景@Component和@Named可互换
 */
//@Component("lonelyHeartsClub")
@Named("lonelyHeartsClub")
public class SgtPeppers implements CompactDisc {
    private  String title=" Sgt. Pepper's Lonely Hearts Club Band";
    private  String artist="The Beatles";
    public void play(){
        System.out.println("Playing "+title+" by "+artist);
    }
}
