package com.wayne.soundsystem;

/**
 * Created by shitou on 17/4/4.
 */

/**
 * CD播放器接口
 */
public interface CompactDisc {
    void play();
}
