package com.wayne.soundsystem;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import javax.inject.Named;

/**
 * Created by shitou on 17/4/20.
 */
//@Component
@Named
public class CDPlayer implements MediaPlayer {
    private CompactDisc cd;

    //@Autowired
    @Inject
    public CDPlayer(CompactDisc cd){
        this.cd=cd;
    }
    public void play(){
        cd.play();
    }
}
