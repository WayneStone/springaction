package com.wayne.model;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

import java.util.Date;
import org.springframework.scheduling.annotation.Scheduled;

/**
 * Created by shitou on 17/5/3.
 */
@Component
@Lazy(false)
public class MyJob {
    @Scheduled(cron="0/5 * *  * * ? ")
    public void work() {
        //System.out.println("date: " + new Date().getTime());
    }
}
